# Loyalty Corp Test

A responsive single page app using Vue, including functionality for sticky elements and carousel features.

## Getting Started

This test was created using vue-cli and comes pre-packaged with easy methods for testing and building this project.

### Prerequisites

If you do not have npm installed already, you can find instructions on how to install it here: https://docs.npmjs.com/getting-started/installing-node

### Installing

First, let's clone our project
```
git clone https://username@bitbucket.org/heinbez/loyalty-corp-test.git
```

Let's install our dependencies, navigate to the root of the cloned project and run the following command
```
npm install
```

You can start a webpack development server with live-reload by running this command:

```
npm run dev
```

this command will start a local-server on your machine that runs on port `1337` - Access this by typing in `http://localhost:1337` in your browser


## Deployment

To deploy our project we have to build our project first, you can do so by running the command:

```
npm run build
```

This command will output the final build of the project within the directory `/dist`.
To deploy this project to your hosting provider of choice, for e.g. `AWS S3 Static`, upload both the file `index.html` and `/dist` directory to the root of your serving directory.

To serve the build project locally, run the command:
```
npm run server
```
This will start a local server on port `1338` using `http-server`

## Built With

* [Vue](https://github.com/vuejs/vue-cli) - framework for single page application
* [Sass](https://sass-lang.com/) - css preprocessor
* [Webpack](https://sass-lang.com/) - build tool

##### Libraries
* [VueCarousel](https://github.com/SSENSE/vue-carousel) - reponsive and mobile ready carousel for vuejs

## Authors

* **Heinrich Bezuidenhout** 