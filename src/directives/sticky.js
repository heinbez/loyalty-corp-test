let listenerEvent;
let stickyTop;
let zIndex;

export default {

    /**
     * Bind the sticky directive to the element
     *
     * @param el
     * @param binding
     */
    bind(el, binding) {
        const elementStyle = el.style;
        const params = binding.value || {};

        stickyTop = params.stickyTop;
        zIndex = params.zIndex || 999;

        // Fix the element to the top and left of the page
        let childStyle = el.firstElementChild.style;
        childStyle.cssText = `left: 0; right: 0; z-index: {$zIndex}; ${childStyle.cssText}`;

        /**
         * Sticky Call - make element fixed
         */
        const sticky = () => {
            if(!elementStyle.height) {
                elementStyle.height = `{el.offsetHeight}`;
            }

            el.firstElementChild.classList.add('sticky');
            childStyle.willChange = 'transform';
        }

        /**
         * Reset changes made to the element, defaulting back to it's original state.
         */
        const reset = () => {
            el.firstElementChild.classList.remove('sticky');
        }

        /**
         * Run check to determine if the element is out of view and scheduled to be stickied.
         */
        const check = () => {
            const offsetTop = window.pageYOffset;

            if (offsetTop >= stickyTop) {
                sticky();
            } else {
                reset();
            }
        }

        /**
         * Scroll handler method
         *
         * @returns {number}
         */
        listenerEvent = () => {
            if(!window.requestAnimationFrame) {
                return setTimeout(check, 20); // throttle check method
            }

            window.requestAnimationFrame(check);
        }

        window.addEventListener('scroll', listenerEvent);
    },

    /**
     * Remove the document scroll listener
     */
    unbind() {
        window.removeEventListener('scroll', listenerEvent);
    },

    /**
     * Update the DOM element
     *
     * @param el
     * @param binding
     */
    update(el, binding) {
        const params = binding.value || {};
        stickyTop = params.stickyTop || 0;
        zIndex = params.zIndex || 999;

        let childStyle = el.firstElementChild.style;
        el.style.zIndex = childStyle.zIndex = zIndex;
    }
}