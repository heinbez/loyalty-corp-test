import Home from './components/Home.vue';
import AboutUs from './components/AboutUs.vue';
import ContactUs from './components/ContactUs.vue';

export default [
    { path: '/', component: Home },
    { path: '/about-us', component: AboutUs },
    { path: '/contact-us', component: ContactUs }
]